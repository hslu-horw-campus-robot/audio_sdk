# audio_sdk package

This sdk has been developed @hslu, based on unitree documentation and code examples. 

## barker.py node
This node subscribes to topic ```/go_bark```. 
If the topic is published in the ROS network, the node can play different audio files that are stored locally.

If the topic ist published from another processing board than head nano, parameter ROS_MASTER_URI and ROS_IP must be set correctly.

## Make go1 barking with ROS node "barker"

### start the node (not required if the node is started automatically)
Ssh to go1 head board and run node barker.py on head nano with commands
```
cd audio_ws
catkin_make
source devel/setup.bash
rosrun audio_sdk barker.py
```

### barking and other sounds

To make go1 output sounds publish topic /go_bark with this command (-1 at the end publishes the topic for 3 seconds)
```rostopic pub /go_bark std_msgs/Int16 <sound_selection> -1```

e.g. for barking -> use value 1
```rostopic pub /go_bark std_msgs/Int16 1 -1```

For <sound_selection> following values can be used:
| value | mode |info |
| ------ | ------ |------ |
| 0      | QUIET       ||
|  1      | BARKING       |some dog barking|
|   2     | WHINING       |some dog whining|
|   3     | SHORT_BARKS       |two short barks|
|    8    | DEMO       |plays 1 and 2 randomly|
|     9   | RANDOM       |like demo, but with longer breaks between sounds|
| 11       | GREETINGS_F       |from here on: some random sounds for workshop|
|   12     | ACCEPTED_M       ||
|     13   | HELLO_M       ||
|       14 | WOW_M       ||


Find latest/current options in file barker.py.



