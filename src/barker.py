#!/usr/bin/python

import sys
import os
import rospy
from std_msgs.msg import Int16
from enum import Enum
from random import random

sys.path.append('../lib/python/amd64')

class Mode(Enum):
    QUIET = 0
    BARKING = 1
    WHINING = 2
    SHORT_BARKS = 3
    DEMO = 8
    RANDOM = 9 # like demo, but with longer breaks between sounds
    GREETINGS_F = 11 # from here on: some random sounds for workshop
    ACCEPTED_M = 12
    HELLO_M = 13
    WOW_M = 14
    CHRISTMAS = 15

mode = Mode(0)

def callback(data):
    global mode
    rospy.loginfo(rospy.get_caller_id() + " got barking request of mode "+str(data.data))
    if data.data == 0:
        mode = Mode.QUIET
    elif data.data == 1:
        mode = Mode.BARKING
    elif data.data == 2:
        mode = Mode.WHINING
    elif data.data == 3:
        mode = Mode.SHORT_BARKS
    elif data.data == 8:
        mode = Mode.DEMO
    elif data.data == 9:
        mode = Mode.RANDOM
    elif data.data == 11:
        mode = Mode.GREETINGS_F
    elif data.data == 12:
        mode = Mode.ACCEPTED_M
    elif data.data == 13:
        mode = Mode.HELLO_M
    elif data.data == 14:
        mode = Mode.WOW_M
    elif data.data == 15:
        mode = Mode.CHRISTMAS
    else:
        mode = Mode.QUIET
        rospy.loginfo("** warning invalid barking mode!")

def barker():
    global mode
    rospy.init_node('barker', anonymous=True)
    rospy.Subscriber('go_bark', Int16, callback)
    pth = os.getcwd()
    rospy.loginfo('barker cwd = ' + pth)
    rate = rospy.Rate(1) # 1 Hz polling for mode changes
    while not rospy.is_shutdown():
        if mode == Mode.QUIET:
            pass
        elif mode == Mode.BARKING:
            # todo check if playing sound is "blocking"
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/little_dog_barking.wav')  # plughw is 0,0 for bebot laptop / 2,0 for go1
            mode = Mode.QUIET
        elif mode == Mode.WHINING:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/whining.wav')
            mode = Mode.QUIET
        elif mode == Mode.SHORT_BARKS:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/short_bark.wav')
            mode = Mode.QUIET
        elif mode == Mode.DEMO:
            choice = random() # Random float:  0.0 <= x < 1.0
            if choice < 0.5:
                pass
            elif choice < 0.75:
                os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/little_dog_barking.wav')
            else:
                os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/whining.wav')
        elif mode == Mode.RANDOM:
            choice = random()
            if choice < 0.8:
                pass
            elif choice < 0.92:
                os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/little_dog_barking.wav')
            else:
                os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/whining.wav')
        elif mode == Mode.GREETINGS_F:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/female-greetings.wav')
            mode = Mode.QUIET
        elif mode == Mode.ACCEPTED_M:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/male-challenge_accepted.wav')
            mode = Mode.QUIET
        elif mode == Mode.HELLO_M:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/male-hello.wav')
            mode = Mode.QUIET
        elif mode == Mode.WOW_M:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/male-wow.wav')
            mode = Mode.QUIET
        elif mode == Mode.CHRISTMAS:
            os.system('aplay -D plughw:2,0 src/audio_sdk/sounds/christmas.wav')
            mode == Mode.QUIET
        else:
            pass
        rate.sleep()

if __name__ == '__main__':
    rospy.loginfo('starting barker node ...')
    barker()
